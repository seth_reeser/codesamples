import argparse
from os import system, environ
import subprocess
from sys import platform as _platform
import win32con
from win32gui import SendMessage
from _winreg import (
    CloseKey, OpenKey, QueryValueEx, SetValueEx,
    HKEY_CURRENT_USER, HKEY_LOCAL_MACHINE,
    KEY_ALL_ACCESS, KEY_READ, REG_EXPAND_SZ, REG_SZ
)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='')
    parser.add_argument('--state',
                        type=str,
                        help='change the state for a specific maintenance mode service',
                        required=False)
    parser.add_argument('--status',
                        action='store_true',
                        help='view the current state of all maintenance mode services',
                        required=False)

parser._optionals.title = "arguments"
args = parser.parse_args()

dictionary_service = {'ORACLE':'SERVICE-AVAILABLE-ORACLE','MAIL':'SERVICE-AVAILABLE-MAIL'}
dictionary_state = {'UP':'1','DOWN':'0','DOWN-ALLOW-INTERNAL':'2'}

def set_env(name, value):
    key = OpenKey(HKEY_LOCAL_MACHINE, 'SYSTEM\\CurrentControlSet\\Control\\Session Manager\\Environment', 0, KEY_ALL_ACCESS)
    SetValueEx(key, name, 0, REG_EXPAND_SZ, value)
    CloseKey(key)
    SendMessage(win32con.HWND_BROADCAST, win32con.WM_SETTINGCHANGE, 0, 'Environment')

def get_env(name):
    try:
        return environ[value]
    except KeyError:
        return 'not set'


print 'maintenance-mode allows you to set a service specific system level environment variable that is used to trigger conditions in code that handle a service disruption'
print '\n'
print 'Key:'
for index, value in dictionary_state.iteritems():
    print '{0} ({1})'.format(value,index)
print '\n'
print 'System environment variable maintenance-mode states:'
print '\n'
for index, value in dictionary_service.iteritems():
    print '\t{0}: ({1}) {2}'.format(index, value, get_env(value))
print '\n'
print 'Modify by using vagrant maintenance-mode --service SERVICE and --state STATE'
print '\n'


if (args.service and args.state):
    print '\n'
    if args.service not in dictionary_service:
        raise Exception("this is not a valid --service, use --status for a list of services")
    elif args.state not in dictionary_state:
        raise Exception("this is not a valid --state, use --status for a list of states")
    else:
        print 'Setting maintenance mode state(s) to:'
        print '\n'
        for index, value in dictionary_service.iteritems():
            if args.service == index:
                if _platform.startswith('linux'):
                    environ[value] = dictionary_state.get(args.state)
                elif _platform == "win32":
                    set_env(value,dictionary_state.get(args.state))
                print '\t==> {0}: ({1}) {2}'.format(index, value, dictionary_state.get(args.state))
            else:
                print '\t{0}: ({1}) {2}'.format(index, value, get_env(value))
        print '\n'
        print 'Restarting services to allow for the system environment variable to be re-read with the new value:'
        print '\n'
        subprocess.call(['TASKKILL', '/F', '/T', '/IM', 'httpd.exe', '/IM', 'tomcat7.exe'])
        subprocess.call(['net', 'start', 'Apache2.2-Zend'])
        subprocess.call(['net', 'start', 'Tomcat7'])
        print '\n'
