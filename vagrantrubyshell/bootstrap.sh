#!/usr/bin/env bash
export DEBIAN_FRONTEND=noninteractive


provisionstart=$(date +%s)
echo -e "\n\n\t\t ===> PROVISION STARTED <===\n\n"
start=$(date +%s)
sudo apt-get -y install git
if [ ! -d /$2/.git ]; then
    git clone --recursive -b master https://seth_reeser@bitbucket.org/deftworx/w-hrend.git /$2
fi
sudo mkdir -p /var/www/
sudo ln -fs /$2 /var/www/
sudo touch /$2/install/install.log
echo "" >> /$2/install/install.log
end=$(date +%s)
echo "[$(date)] PROVISION STARTED ($(($end - $start)) seconds)" >> /$2/install/install.log


echo -e "\n\n\t\t ===> Updating Existing Packages <===\n\n"
start=$(date +%s)
sudo apt-get update 
end=$(date +%s)
echo "[$(date)] Updating Existing Packages ($(($end - $start)) seconds)" >> /$2/install/install.log


echo -e "\n\n\t\t ===> Installing MySQL <===\n\n"
start=$(date +%s)
sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password password pass@123'
sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password_again password pass@123'
sudo apt-get -y install mysql-server-5.5
end=$(date +%s)
echo "[$(date)] Installing MySQL ($(($end - $start)) seconds)" >> /$2/install/install.log


echo -e "\n\n\t\t ===> Configuring MySQL <===\n\n"
start=$(date +%s)
sed "s/!software!/${2}/g" /$2/install/mysql.sql.template > /$2/install/mysql.sql
sudo cat /$2/install/mysql.sql | mysql -u root -ppass@123
end=$(date +%s)
echo "[$(date)] Configuring MySQL ($(($end - $start)) seconds)" >> /$2/install/install.log


echo -e "\n\n\t\t ===> Installing Apache <===\n\n"
start=$(date +%s)
sudo apt-get install -y apache2
if ! grep -q "ServerName localhost" "/etc/apache2/apache2.conf"; then
   sudo bash -c 'echo "ServerName localhost" >> /etc/apache2/apache2.conf'
fi
end=$(date +%s)
echo "[$(date)] Installing Apache ($(($end - $start)) seconds)" >> /$2/install/install.log


echo -e "\n\n\t\t ===> Configuring Apache <===\n\n"
start=$(date +%s)
sudo a2enmod ssl
sudo a2enmod rewrite
sudo mkdir -p /var/log/apache2/$2
sudo touch /var/log/apache2/$2/access.log
sudo touch /var/log/apache2/$2/error.log
sudo cat > /etc/apache2/sites-available/$2.conf << EOF

RewriteEngine On

<VirtualHost *:80>

    ServerAdmin admin@example.com
    ServerName $3
    ServerAlias www.$3
    DocumentRoot /var/www/$2/wwwroot
    ErrorLog /var/log/apache2/$2/error.log
    CustomLog /var/log/apache2/$2/access.log combined

    # Rewrite all traffic to ssl
    Redirect Permanent / https://www.$3/

</VirtualHost>

<IfModule mod_ssl.c>
    <VirtualHost _default_:443>

        ServerAdmin admin@example.com
        ServerName $3
        ServerAlias www.$3
    	DocumentRoot /var/www/$2/wwwroot
        Options -Indexes +FollowSymlinks
	    ErrorLog /var/log/apache2/$2/error.log
	    CustomLog /var/log/apache2/$2/access.log combined

        # Rewrite all traffic to www.
        RewriteCond %{HTTP_HOST} !^www\.
        RewriteRule (.*) https://www.%{HTTP_HOST}%{REQUEST_URI} [R=301]

        SSLEngine on
        SSLCertificateFile /etc/ssl/certs/ssl-cert-snakeoil.pem
        SSLCertificateKeyFile /etc/ssl/private/ssl-cert-snakeoil.key
        <FilesMatch "\.(cgi|shtml|phtml|php)$">
            SSLOptions +StdEnvVars
        </FilesMatch>
        <Directory /usr/lib/cgi-bin>
            SSLOptions +StdEnvVars
        </Directory>
        BrowserMatch "MSIE [2-6]" \
            nokeepalive ssl-unclean-shutdown \
            downgrade-1.0 force-response-1.0
        BrowserMatch "MSIE [17-9]" ssl-unclean-shutdown

        # Rewrite HTTP Errors
        # bad request
        ErrorDocument 400 /
        # unauthorized
        ErrorDocument 401 /
        # forbidden
        ErrorDocument 403 /
        # not found
        ErrorDocument 404 /
        # internal server error
        ErrorDocument 500 /

    </VirtualHost>
</IfModule>

EOF
sudo a2ensite $2.conf
cd /$2/wwwroot/images/
git update-index --assume-unchanged logo-50px.png
sudo chown -R www-data:www-data /$2/wwwroot/images/logo-50px.png
end=$(date +%s)
echo "[$(date)] Configuring Apache ($(($end - $start)) seconds)" >> /$2/install/install.log


echo -e "\n\n\t\t===> Installing PHP <===\n\n"
start=$(date +%s)
sudo apt-get install -y php5
sudo apt-get install -y php5-curl
sudo apt-get install -y php5-mysql
end=$(date +%s)
echo "[$(date)] Installing PHP ($(($end - $start)) seconds)" >> /$2/install/install.log


echo -e "\n\n\t\t===> Installing Composer <===\n\n"
start=$(date +%s)
sudo php -r "readfile('https://getcomposer.org/installer');" | php -- --install-dir=/$2/wwwroot/libraries/composer
cd /$2/wwwroot/libraries/composer/
git update-index --assume-unchanged composer.phar
sudo php composer.phar update
sudo cat /$2/wwwroot/libraries/composer/vendor/cartalyst/sentry/schema/mysql.sql | mysql -u root -ppass@123 $2
sudo php /$2/install/cartalyst-sentry.php
end=$(date +%s)
echo "[$(date)] Installing Composer ($(($end - $start)) seconds)" >> /$2/install/install.log


echo -e "\n\n\t\t===> Restarting Apache <===\n\n"
start=$(date +%s)
sudo service apache2 restart
end=$(date +%s)
echo "[$(date)] Restarting Apache ($(($end - $start)) seconds)" >> /$2/install/install.log


provisionend=$(date +%s)
echo -e "\n\n\t\t===> PROVISION COMPLETE ($(($provisionend - $provisionstart)) seconds) <===\n\n"
echo "[$(date)] PROVISION COMPLETE ($(($provisionend - $provisionstart)) total seconds)" >> /$2/install/install.log
