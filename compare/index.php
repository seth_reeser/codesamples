<!DOctyPE html>
<html>
<head>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
    <link href="http://fonts.googleapis.com/css?family=Titillium+Web:200" rel="stylesheet" type="text/css">
    <style type="text/css">
        .clearfix:before,
        .clearfix:after {
            content: " "; /* 1 */
            display: table; /* 2 */
        }
        .clearfix:after {
            clear: both;
        }
        .clearfix {
            *zoom: 1;
        }
        * {
            margin:0;
            padding:0;
        }
        body, html {
            background-color:black;
            color:white;
            font:10px serif;
            height:100%;
            width:100%;
        }
        #dashboard div {
        }
        #title {
            font:5em 'Titillium Web';
            padding:2% 0 0 2%;
        }
        #title img {
            display:none;
        }
        #controls {
            font:1.5em 'Titillium Web';
            padding:0 0 2% 2%;
        }
        #cred {
            font:2em 'Titillium Web';
            position:absolute;
            top:0;
            padding:3% 3% 0 0;
            right:0;
        }
        .error {
            color:red;
        }
        #wrapper {
            clear:both;
            padding-bottom:2em;
            width:100%;
        }
        #wrapper div {
            background-color:black;
            float:left;
            height:100%;
            width:20%;
        }
        #wrapper .error {
            font:1.5em 'Titillium Web';
        }
        .bg-match {
            background-color:green;
        }
        .bg-mismatch-md5 {
            background-color:red;
        }
        .bg-mismatch-filename {
            background-color:gray;
        }
        button, select {
            color:white;
            cursor:pointer;
            background-color:black;
            border:1px solid gray;
            font-family:'Titillium Web';
            margin:4px;
            padding:2px 4px;
        }
        h1 {
            font-family:'Titillium Web';
            height:32px;
        }
        ul {
            list-style-type:none;
        }
        ul li {
            white-space:nowrap;
        }
    </style>
</head>
<body>
<div id="dashboard">
    <div id="title">
        EnviroCheck <!--7930.25(pie)--> <!--Remove cleverness for Corporate adoption-->
        <img src="load.gif">
    </div>
    <div id="controls">
        <select id="control-directory">
        </select>
        <select id="control-filetype">
        </select>
        <button id="gotime-button">Go!</button>
        <span id="control-error" class="error"></span>
    </div>
    <div id="cred">
        A Product of Seth Reeser
    </div>
</div>
<div id="wrapper" class="clearfix">
</div>
<script>

    var servers = ["test"];
    var directory = encodeURIComponent("c:\\institutesRoot");
    var root = encodeURIComponent("c:\\institutesRoot");

    var filetypes = ["*.htm","*.php","*.js","*.css","*.ini","*.*"];
    var filetypeselected;

    $(document).ready(function() {

        $.getJSON('http://www.theinstitutes.org/apps/compare/directories.php?root='+directory+'&callback=?', function(data) {
            directories = $.parseJSON(JSON.stringify(data));
            $.each(directories, function(index, directory){
                $("#control-directory").append("<option class="+directory+">"+directory+"</option>");
            });
        });
        $("#control-directory").change(function(){
            root = encodeURIComponent($(this).val());
        });

        $.each(filetypes, function(index, filetype) {
            $("#control-filetype").append("<option value="+filetype.replace(/\W/g, '')+">"+filetype+"</option>");
        });
        filetypeselected = $("#control-filetype").val();
        $("#control-filetype").change(function() {
            filetypeselected = $(this).val();
        });

        $("#gotime-button").click(function(){
            $("#wrapper, #control-error").empty();
            $("#title img").fadeIn();
            $.each(servers, function(index, server) {
                $('#wrapper').append("<div id="+server+"></div>");
            });
            gotime();
        });

        window.gotime = function() {
            $.getJSON('http://www.theinstitutes.org/apps/compare/compare_new.php?root='+root+'&filetype='+filetypeselected+'&callback=?', function(data) {
                dataaiphp2 = $.parseJSON(JSON.stringify(data));
                if (dataaiphp2 === null) {
                    $("#control-error").hide().html("No data").fadeIn(500);
                }
                else {
                    $('#wrapper').append("<div id=\"aiphp2\"></div>");
                    $('#aiphp2').append("<h1>production</h1>");
                    var countFolder;
                    $.each(dataaiphp2, function(index){
                        countFolder = index.replace(/\W/g, '');
                        $("#aiphp2").append("<ul class="+countFolder+"><li>"+index+"</li></ul>");
                        $.each(this, function(index, value){
                            $("#aiphp2 ."+countFolder).append("<li>&nbsp;&nbsp;"+value['filename']+"</li>");
                        })
                    })
                }
            })
            .done(function(data){
                if (dataaiphp2 === null) {
                    $("#title img").fadeOut(1000);
                }
                else {
                    var countServer = 0;
                    $.each(servers, function(index, server) {
                        $.getJSON('http://'+server+'.theinstitutes.org/apps/compare/compare_new.php?root='+root+'&filetype='+filetypeselected+'&callback=?', function(data) {
                            $('#'+server).append("<h1>"+server+"</h1>");
                            window[server] = $.parseJSON(JSON.stringify(data));
                            if (window[server] === null) {
                                $("#"+server).append("<span class='error'>This host does not contain the selected folder.</span>");
                            }
                            else {
                                var countFolder;
                                $.each(window[server], function(index){
                                    countFolder = index.replace(/\W/g, '');
                                    testfolder = index;
                                    $.each(dataaiphp2, function(index){
                                        console.log(index);
                                    });
                                    $("#"+server+"").append("<ul class="+countFolder+"><li>"+index+"</li></ul>");
                                    $.each(this, function(index, value){
                                        testfilename = value['filename'];
                                        testmd5 = value['md5'];
                                        testclass = JSON.stringify(testfilename).replace(/\W/g, '')+testmd5;
                                        $("#"+server+" ."+countFolder).append("<li class='"+testclass+" bg-mismatch-filename'>&nbsp;&nbsp;"+value['filename']+"</li>");
                                        $.each(dataaiphp2, function(){
                                            $.each(this, function(index, value){
                                                if (testfilename === value['filename'] && testmd5 === value['md5']) {
                                                    $('#'+server+' .'+testclass).removeClass('bg-mismatch-filename');
                                                    $('#'+server+' .'+testclass).addClass('bg-match');
                                                }
                                                if (testfilename === value['filename'] && testmd5 !== value['md5']) {
                                                    $('#'+server+' .'+testclass).removeClass('bg-mismatch-filename');
                                                    $('#'+server+' .'+testclass).addClass('bg-mismatch-md5');
                                                }
                                            })
                                        })
                                    })
                                })
                            }
                        })
                        .done(function(){
                            countServer = countServer + 1;
                            if (servers.length === countServer) {
                                $("#title img").fadeOut(1000);
                            }
                            var countClass;
                            $("#"+server+" ul").each(function(index){
                                countClass = $(this).attr('class');
                                var classHeight = 0;
                                $("."+countClass).each(function(){
                                    if ($(this).height() > classHeight){
                                        classHeight = $(this).height();
                                    }
                                })
                                //console.log(countClass+" --- "+classHeight);
                                $("."+countClass).animate({'height':classHeight});
                            });
                        });
                    });
                }
            });
        }
    });

</script>
</body>
</html>
